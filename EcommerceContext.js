
import  { createContext, useState } from 'react';
 
const EcommerceContext = createContext(null);
 
export default EcommerceContext;
