import { useState, useEffect, useContext } from 'react';
import { Navbar, Nav, Container, Modal, Button, Form, Row, Col } from 'react-bootstrap';
import { Outlet, Link, useNavigate } from "react-router-dom";
import EcommerceContext from "../EcommerceContext";
import { FaShoppingCart } from 'react-icons/fa'
import { GiShoppingBag } from 'react-icons/gi'
import jwt_decode from "jwt-decode";
import { login } from './tools/Api';


import './App.css'

function App() {
  const [token, setToken] = useState('');
  const [username, setUsername] = useState('');

  const [show, setShow] = useState(false);
  const [email, setEmail] = useState('');
  const [error, setError] = useState(false);
  const [password, setPassword] = useState('');

  const [carroCompra, setCarroCompra] = useState([]);
  const [precioFinal, setPrecioFinal] = useState(0);

  const [ensenyar, setEnsenyar] = useState(false);

  const handleCloseCarro = () => setEnsenyar(false);
  const handleShowCarro = () => setEnsenyar(true);

  const Ecommerce = useContext(EcommerceContext);

  const navigateTo = useNavigate();
  const goHome = () => {
    navigateTo('/')
  };
  const goTramite = () => {
    navigateTo('/tramitePedido')
    setEnsenyar(false)
  }
  const goDetalle = () => {
    navigateTo('/detalle')
  };


  useEffect(() => {
    const tk = localStorage.getItem('loginfront');
    if (tk) {
      setToken(tk)
    }
  }, [])

  useEffect(() => {
    if (token) {
      const decoded = jwt_decode(token);
      setUsername(decoded.nom);
      localStorage.setItem('loginfront', token);
    } else {
      setUsername('');
    }

    goHome();
  }, [token])


  const handleLogin = () => {

    login(email, password)
      .then(resp => {
        if (resp.ok === true) {
          setToken(resp.token);
          setShow(false);
        } else {
          console.log("resp", resp)
          setError(resp.msg);
        }
      })
      .catch(e => setError(e))
  };

  function logout() {
    setToken('');
    localStorage.removeItem('loginfront');
  }
  // LOG IN:
  // {username ?
  //   <>
  //     <span className='nav-item pointer' onClick={logout}>Logout</span>
  //     <span className="nav-item ms-1">({username})</span>
  //   </>
  //   : <span onClick={() => setShow(true)} className="nav-item pointer">Login</span>
  // }
  return (
    <>
      <EcommerceContext.Provider value={{ token, username, carroCompra, setCarroCompra, precioFinal, setPrecioFinal, ensenyar, setEnsenyar, handleCloseCarro, handleShowCarro, goHome, goTramite }}>
        <Navbar bg="secondary" variant="dark" expand="lg">
          <Container>
            <Link to="/" className="navbar-brand">HOME</Link>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
              <Nav className="me-auto">
                <Link to="/subirArticulo" className='nav-link'>Nuevo artículo</Link>
              </Nav>
              <Nav className="me-auto">
                <Link to="/stockFotos" className='nav-link'>Editar Stock</Link>
              </Nav>
            </Navbar.Collapse>
            <div>
              <span className="cestaCompra" onClick={() => setEnsenyar(true)}><GiShoppingBag /><div className="cantidadCarro"><span>{carroCompra.length}</span></div></span>
            </div>
          </Container>
        </Navbar>
        <Row>
          <Col>
            <Container>
              <Outlet />
            </Container>
          </Col>
        </Row>
      </EcommerceContext.Provider>

      <Modal show={show} >
        <Modal.Header >
          <Modal.Title>Login required</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form>
            <Form.Group className="mb-3" controlId="formName">
              <Form.Label>Email</Form.Label>
              <Form.Control type="text" value={email} onInput={(e) => setEmail(e.target.value)} />
            </Form.Group>
            <Form.Group className="mb-3" controlId="formPassword">
              <Form.Label>Password</Form.Label>
              <Form.Control type="password" value={password} onInput={(e) => setPassword(e.target.value)} />
            </Form.Group>
          </Form>
          {error ? <span className="error-msg">{error}</span> : <span>Enter credentials</span>}
        </Modal.Body>
        <Modal.Footer>
          <Button variant="primary" onClick={handleLogin}>
            Login
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  )
}

export default App


