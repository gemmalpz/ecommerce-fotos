import { useContext, useEffect, useState } from "react";
import EcommerceContext from "../../EcommerceContext";
import { useNavigate } from "react-router-dom";
import './subirarticulo.css';
import { Button, Row, Col, Form } from 'react-bootstrap';

import { ToastContainer, toast } from "react-toastify";


const API_URL = "http://localhost:3030/api";

function subirArticulo() {

    const Ecommerce = useContext(EcommerceContext);

    const [arxiu, setArxiu] = useState(null);
    const [titol, setTitol] = useState('');
    const [categoria, setCategoria] = useState('');

    const onChangeHandler = event => {
        setArxiu(event.target.files[0]);
    };

    const submit = (e) => {
        e.preventDefault();

        const data = new FormData()
        data.append('file', arxiu);
        data.append('titol', titol);
        data.append('categoria', categoria);

        const opciones = {
            method: 'POST',
            body: data
        };

        fetch(API_URL + "/article", opciones)
            .then(res => res.json())
            .then(res => {
                if (res.ok === true) {
                    toast("Article nou penjat", { onClose: () => Ecommerce.goHome() });
                } else {
                    console.log(res);
                    toast("Error: " + res.error);
                }
            })
    }

    useEffect(()=>{
        if (arxiu){
          setCategoria("A")
        }
      },[arxiu]);

    return (
        <>
            <ToastContainer />
            <br />
            <h2 className="tituloPagina">Subir nuevo artículo</h2>
            <Form onSubmit={submit} encType='multipart/form-data' >
                <Form.Group className="mb-3" controlId="formName">
                    <Form.Label>Titol</Form.Label>
                    <Form.Control type="text" value={titol} onInput={(e) => setTitol(e.target.value)} />
                </Form.Group>
                <Form.Group className="mb-3" controlId="formName">
                    <Form.Label>Categoria</Form.Label>
                    <Form.Select type="text" value={categoria} onInput={(e) => setCategoria(e.target.value)}>
                        <option>A</option>
                        <option>B</option>
                        <option>C</option>
                    </Form.Select>
                </Form.Group>
                <Row>
                    <Col sm="6">
                        <Form.Group controlId="formFile" className="mb-3">
                            <Form.Label>Default file input example</Form.Label>
                            <Form.Control type="file" name="file" onChange={onChangeHandler} />
                        </Form.Group>
                    </Col>
                </Row>

                <Row>
                    <Col>
                        <span>
                            <button type="button" className='boton' onClick={() => Ecommerce.goHome()} size='sm' color="danger" >{"Sortir sense desar"}</button>
                            {' '}
                            <button type="submit" className='boton' size='sm' color="success" >{"Desar canvis"}</button>
                        </span>
                    </Col>
                </Row>

            </Form>
        </>
    );
};

export default subirArticulo;
