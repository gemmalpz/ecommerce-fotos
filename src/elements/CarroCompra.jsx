import { useContext, useEffect, useState } from "react";
import EcommerceContext from "../../EcommerceContext";
import './carroCompra.css';
import { FaRegTrashAlt, FaShoppingCart } from 'react-icons/fa'
import { GiShoppingBag } from 'react-icons/gi'
import { Button, Row, Col, Modal, Table, Offcanvas } from 'react-bootstrap';

function CarroCompra() {

    const Ecommerce = useContext(EcommerceContext);

    useEffect(() => {
        let importeTotal = 0;
        Ecommerce.carroCompra.forEach((el) => {
            importeTotal += el.subtotal;
        });
        Ecommerce.setPrecioFinal(importeTotal.toFixed(2))
    }, [Ecommerce.carroCompra]);

    function visualizarCarro(el, idx) {

        el.subtotal = el.precio + el.formatoPrecio

        return (
            <tr key={idx}>
                <td><img src={el.imagen} alt="x" width={80} /></td>
                <td className="formato">{el.precio} €</td>
                <td><span className="formato">{el.formato} cm</span><br /><span className="formatoPrecio">{el.formatoPrecio.toFixed(2)} €</span></td>
                <td><span className="subtotal">{el.subtotal.toFixed(2)} €</span></td>
                <td className="casillaIcono"><span><FaRegTrashAlt className="iconoTrash" onClick={() => eliminarDelCarro(el)}/></span></td>
            </tr>
        )
    };

    function eliminarDelCarro(el) {
        let index = Ecommerce.carroCompra.indexOf(el);
        let nuevoCarro = [...Ecommerce.carroCompra]
        nuevoCarro.splice(index, 1)
        Ecommerce.setCarroCompra(nuevoCarro);
    }

    const filasCarro = Ecommerce.carroCompra.map(visualizarCarro);

    return (
        <>
            <Offcanvas show={Ecommerce.ensenyar} onHide={Ecommerce.handleCloseCarro} placement={'end'} >

                {Ecommerce.carroCompra.length >=1 ?
                    <>
                        <Offcanvas.Header>
                            <Offcanvas.Title><span className="iconoCarro"><GiShoppingBag /></span>Mi cesta</Offcanvas.Title>
                        </Offcanvas.Header>
                        <Offcanvas.Body>
                            <Table responsive>
                                <thead>
                                    <tr>
                                    </tr>
                                </thead>
                                <tbody>
                                    {filasCarro}
                                </tbody>
                                <tfoot>
                                    <tr className="precioFinal"><td></td><td></td><td></td><td></td><td>{Ecommerce.precioFinal}€</td></tr>
                                </tfoot>
                            </Table>
                            <button className="seguirComprando" onClick={() => Ecommerce.handleCloseCarro()}> Seguir comprando </button>
                            <button className="tramitarPedido" onClick={() => Ecommerce.goTramite()}> Tramitar Pedido </button>
                        </Offcanvas.Body>
                    </>

                    : 
                    
                    <>
                        <Offcanvas.Header>
                            <Offcanvas.Title><span className="iconoCarro"><GiShoppingBag /></span>Mi cesta</Offcanvas.Title>
                        </Offcanvas.Header>
                        <Offcanvas.Body>
                            <h6 className="cestaVacia">Tu cesta está vacía</h6>
                            <button className="seguirComprando" onClick={() => Ecommerce.handleCloseCarro()}> Añadir imágenes </button>
                        </Offcanvas.Body>

                    </>}
            </Offcanvas>
        </>
    );
};

export default CarroCompra;
