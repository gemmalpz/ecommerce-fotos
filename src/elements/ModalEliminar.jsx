import { Button, Row, Col, Modal, ToggleButton, ToggleButtonGroup, ButtonGroup } from 'react-bootstrap';
import { useState, useEffect } from 'react';
import { Navigate, useParams } from 'react-router-dom';
import { FaRegTrashAlt, FaShoppingCart } from 'react-icons/fa'

import './modaleliminar.css'

const APIURL = "http://localhost:3030/api"

function ModalEliminar({id, titol, getArticulos}) {


    const ARTICULO = id;

    const titulo = titol;

    const [modalShow, setModalShow] = useState(false);

    function borrar() {
        const opciones = {
            "method": "DELETE"
        }

        fetch(APIURL + "/article/" + ARTICULO, opciones)

            .then(x => x.json())
            .then(x => setModalShow(false))
            .then(x => getArticulos())
            .catch(x => console.log(x))
    }


    return (
        <>
            <span onClick={() => setModalShow(true)} className="iconoBasura"><FaRegTrashAlt /></span>
            <Modal
                show={modalShow}
                onHide={() => setModalShow(false)}
                size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                centered
            >
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-vcenter">
                        Eliminar artículo
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <h4>¿Deseas eliminar el artículo</h4>
                </Modal.Body>
                <Modal.Footer>
                    <button className="eliminar" onClick={()=>borrar()}>Eliminar</button>
                    <button className="cancelar" onClick={()=> setModalShow(false)}>Cancelar</button>
                </Modal.Footer>
            </Modal>
        </>
    )
}

export default ModalEliminar;