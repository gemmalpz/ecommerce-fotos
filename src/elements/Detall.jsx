
import { useContext, useEffect, useState } from "react";
import EcommerceContext from "../../EcommerceContext";
import { useNavigate, useParams } from "react-router-dom";
import { Button, Row, Col, Form, Container } from 'react-bootstrap';
import './detall.css';
import { ToastContainer, toast } from "react-toastify";

import { API_URL, IMG_URL } from './config';
import { TitolContainer } from './styled_common';
import ImageUpload from "./ImageUpload.jsx";


function Detall() {

    const { id } = useParams();

    const navigateTo = useNavigate();
    const goTo = (x) => {
        navigateTo(x)
    };

    const [foto, setFoto] = useState(null);
    const [titol, setTitol] = useState('');
    const [categoria, setCategoria] = useState('');
    const [article, setArticle] = useState(null);


    function getArticle(x) {
        const laurl = API_URL + "/article/" + x;
        console.log("anant a :", laurl)
        fetch(laurl)
            .then(results => results.json())
            .then(results => setArticle(results.data))
            .catch(err => console.log(err));
    }

    useEffect(() => {
        getArticle(id);
    }, [])

    useEffect(() => {
        if (article) {
            setTitol(article.titol);
            setCategoria(article.categoria);
        }
    }, [article])


    const submit = (e) => {
        e.preventDefault();

        const data = new FormData()
        data.append('file', foto);
        data.append('titol', titol);
        data.append('categoria', categoria);

        const opciones = {
            method: 'PUT',
            body: data
        };

        fetch(API_URL + "/article/" + id, opciones)
            .then(res => res.json())
            .then(res => {
                if (res.ok === true) {
                    toast.success("Article modificat", { onClose: () => goTo('/stockFotos') });
                } else {
                    console.log(res);
                    toast.error("Error: " + res.error);
                }
            })
    }

    return (
        <>
            <ToastContainer />
            <h2 className="tituloPagina">Edita imagen</h2>
            <Form onSubmit={submit} encType='multipart/form-data' >

                <Form.Group className="mb-3" controlId="formName">
                    <Form.Label>Titol</Form.Label>
                    <Form.Control type="text" value={titol} onChange={(e) => setTitol(e.target.value)} />
                </Form.Group>


                <Form.Group className="mb-3" controlId="formName">
                    <Form.Label>Categoria</Form.Label>
                    <Form.Select type="text" value={categoria} onChange={(e) => setCategoria(e.target.value)}>
                        <option>A</option>
                        <option>B</option>
                        <option>C</option>
                    </Form.Select>
                </Form.Group>

                <Col sm="6">
                    <p>Img actual:</p>
                    <span className="imatgeActual">{article && article.foto ? <img src={IMG_URL + article.foto} width={"100px"} /> : <></>}</span>
                </Col>
                <Row>
                    <Col sm="6">
                        <ImageUpload useFoto={[foto, setFoto]} />
                    </Col>
                </Row>
                <Row>
                    <span className="float-right">
                        <button type="button" onClick={() => goTo('/stockFotos')} className='cancelar'>{"Sortir sense desar"}</button>
                        {' '}
                        <button type="submit" className='guardar'>{"Desar canvis"}</button>
                    </span>
                </Row>
            </Form>
        </>
    );
}
export default Detall;
