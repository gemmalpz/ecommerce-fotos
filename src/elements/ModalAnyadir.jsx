import { Button, Row, Col, Modal, ToggleButton, ToggleButtonGroup, ButtonGroup } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import './modalanyadir.css';
import EcommerceContext from "../../EcommerceContext";



function ModalAnyadir({ img, categoria, titol, precio }) {
  
  const [Show, setShow] = useState(false);

  const handleShow = () => setShow(true);
  const handleClose = () => setShow(false);

  const [seleccionado, setSeleccionado] = useState(false)

  const [formato, setFormato] = useState("")

  const Ecommerce = useContext(EcommerceContext);


  const formatos = [
    { id: 1, formato: "20x30", precio: 24.99 },
    { id: 2, formato: "30x40", precio: 34.99 },
    { id: 3, formato: "40x50", precio: 40.99 },
    { id: 4, formato: "40x60", precio: 49.99 },
    { id: 5, formato: "50x75", precio: 54.99 },
    { id: 6, formato: "60x80", precio: 59.99 },
    { id: 7, formato: "60x90", precio: 84.99 },
    { id: 8, formato: "75x100", precio: 99.99 }
  ]

   useEffect(()=>{
     if (Show){
       setFormato(formatos[0])
     }
   },[Show]);

  function escogerFormato(n){
    setFormato(n)
    setSeleccionado(true)
  };
  
  function opcionesFormatos(el ,idx) {
    return (
      <button
            key={idx}
            className= {formato.id === el.id? "activo" : "tamanyos"}
            value={el.precio.toFixed(2)}
            onClick={() => escogerFormato(el)}
          >
            {el.formato}
      </button>
    )
  }

const formatosDisponibles = formatos.map(opcionesFormatos);

const anyadirAlCarro = () => {
    Ecommerce.setCarroCompra([
      ...Ecommerce.carroCompra,
      {
        precio: precio,
        imagen: img,
        formatoPrecio: formato.precio,
        formato: formato.formato,
      },
    ]);
    handleClose()
    Ecommerce.handleShowCarro()
  };

return (
  <>
    <button className="boton" onClick={() => setShow(true)} > Add </button>

    <Modal
      size="lg"
      aria-labelledby="modalAnyadirAlCarro"
      centered
      show={Show}
      onHide={handleClose}
    >
      <Modal.Header closeButton>
        <Modal.Title id="modalAnyadirAlCarro">
          Comprar {titol}
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Row>
          <Col>
            <img src={img} alt="x" width={300}/>
          </Col>
          <Col>
            <h5>Selecciona el tamaño de impresión (cm):</h5>
            <div type="checkbox" className="btn-group" name="formatos">
              {formatosDisponibles}
            </div>
            <div className="precioImagen">
              Precio foto: {precio} €
              <br/>
              Precio formato: {formato.precio} €
              
            </div>
            <div className="subtotal">
              { formato ? 
              <span>Precio total de la pieza: {(precio+formato.precio).toFixed(2)} €</span>
              : <></>}
            </div>
          </Col>
        </Row>
      </Modal.Body>
      <Modal.Footer>
        <button className="botonModal" onClick={handleClose}>Close</button>
        <button className="botonModal" onClick={anyadirAlCarro}>Añadir al carrito</button>
      </Modal.Footer>
    </Modal>
  </>
)

}

export default ModalAnyadir;