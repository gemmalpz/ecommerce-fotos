import { Button, Card, Row, Form, Modal, Container, ButtonGroup } from 'react-bootstrap';
import { FaRegTrashAlt, FaShoppingCart } from 'react-icons/fa'
import './inici.css'
import { useState, useEffect, useContext } from 'react';
import EcommerceContext from "../../EcommerceContext";

import ModalAnyadir from './ModalAnyadir';
import CarroCompra from './CarroCompra';

const URL = "http://localhost:3030";

function getFotoUrl(x) {
  return URL + '/img/' + x;
}

export default function Inici() {

  const Ecommerce = useContext(EcommerceContext);

  const [articulos, setArticulos] = useState([]);

  function MostraFoto({ foto }) {
    return (
      <img src={URL + '/img/' + foto} alt="x" width={"300px"} />
    );
  };

  function getArticulos() {
    fetch(URL + "/api/article")
      .then(results => results.json())
      .then(results => setArticulos(results.data))
      .catch(err => console.log(err));
  }

  useEffect(() => {
    getArticulos();
  }, [])

  if (!articulos.length) {
    return <>...</>;
  }

  const categorias = [
    { id: 1, nombre: "A", precio: 59.99 },
    { id: 2, nombre: "B", precio: 39.99 },
    { id: 3, nombre: "C", precio: 19.99 }
  ]

  //ELEMENT CARTA
  function carta(carta, idx) {

    carta.preu = 0

    if (carta.categoria === "A") {
      carta.preu = carta.preu + categorias[0].precio
    } else
      if (carta.categoria === "B") {
        carta.preu = carta.preu + categorias[1].precio
      } else
        if (carta.categoria === "C") {
          carta.preu = carta.preu + categorias[2].precio
        };

    return (
      <Card className="articulo" key={idx} style={{ width: 300 }}>
        <Card.Img src={getFotoUrl(carta.foto)} alt="xx" />
        <Card.Body>
          <Card.Title>{carta.titol}</Card.Title>
          <Card.Text>
            {carta.preu} €
          </Card.Text>
          <ModalAnyadir img={getFotoUrl(carta.foto)} categoria={carta.categoria} titol={carta.titol} precio={carta.preu} />
        </Card.Body>
      </Card>
    )
  };

  const cartasArticulos = articulos.map(carta);

  return (
    <>
      <h2 className="tituloPagina">Banco de imágenes</h2>
      <Row>
        {cartasArticulos}
      </Row>
      <CarroCompra />
    </>
  );
}
