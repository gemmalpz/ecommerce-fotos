import { Button, Row, Table, Modal } from 'react-bootstrap';
import { FaRegTrashAlt, FaShoppingCart } from 'react-icons/fa'
import { AiOutlineEdit } from 'react-icons/ai'

import './stockfotos.css';

import { useState, useEffect, useContext } from 'react';
import { useNavigate, Link, useParams } from "react-router-dom";

import EcommerceContext from "../../EcommerceContext";

import ModalEliminar from './ModalEliminar';

const URL = "http://localhost:3030";

function getFotoUrl(x) {
    return URL + '/img/' + x;
}

export default function StockFotos() {

    const Ecommerce = useContext(EcommerceContext);

    const [articulos, setArticulos] = useState([]);

    const { id } = useParams();

    function getArticulos() {
        fetch(URL + "/api/article")
            .then(results => results.json())
            .then(results => setArticulos(results.data))
            .catch(err => console.log(err));
    }

    useEffect(() => {
        getArticulos();
    }, [])

    if (!articulos.length) {
        return <>...</>;
    }

    const categorias = [
        { id: 1, nombre: "A", precio: 59.99 },
        { id: 2, nombre: "B", precio: 39.99 },
        { id: 3, nombre: "C", precio: 19.99 }
    ]

    //ELEMENT FILA DE LA TABLA
    function fila(carta, idx) {

        carta.preu = 0

        if (carta.categoria === "A") {
            carta.preu = carta.preu + categorias[0].precio
        } else
            if (carta.categoria === "B") {
                carta.preu = carta.preu + categorias[1].precio
            } else
                if (carta.categoria === "C") {
                    carta.preu = carta.preu + categorias[2].precio
                };

        return (
            <tr key={idx}>
                <td> <img src={getFotoUrl(carta.foto)} alt="xx" width={"80px"} /> </td>
                <td> {carta.titol} </td>
                <td>{carta.categoria}</td>
                <td>{carta.preu} €</td>
                <td>
                    <ModalEliminar getArticulos={getArticulos} id={carta.id} titulo={carta.titol} />
                </td>
                <td><Link to={"/detall/" + carta.id} className="iconoEditar"><AiOutlineEdit /></Link></td>
            </tr>
        )
    };

    const cartasArticulos = articulos.map(fila);

    //ELIMINAR ARTICLE > MODAL

    //get articulo en concreto
    

    return (
        <>
            <h2 className="tituloPagina"> Stock y control de imágenes</h2>
            <Link to="/" className="botonBack">Volver</Link>
            <Row>
                <Table responsive>
                    <thead>
                        <tr>
                            <th>Imagen</th>
                            <th>Título</th>
                            <th>Categoría</th>
                            <th>Precio</th>
                        </tr>
                    </thead>
                    <tbody>
                        {cartasArticulos}
                    </tbody>
                </Table>
            </Row>

        </>
    );
}
