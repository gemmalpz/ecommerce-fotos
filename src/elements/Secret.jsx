
import { useContext, useEffect, useState } from "react";
import EcommerceContext from "../../EcommerceContext";

const API_URL = "http://localhost:3030/api";


export default () => {
  const [resposta, setResposta] = useState('...');

  const Ecommerce = useContext(EcommerceContext);
 
  useEffect(() => {

    const requestOptions = {
      method: 'GET',
      headers: { 'Content-Type': 'application/json', authorization: User.token },
    };
  
    return fetch(API_URL + "/usuari/secret", requestOptions)
      .then(response => response.json())
      .then(response => {
        if (response.ok){
          setResposta(response.data);
        }else{
          setResposta('ERROR')
        }
      })
      .catch(error => console.log(error));
    
  }, []);

 

  return (
    <main style={{ padding: "1rem 0" }}>
    <h2>Secret...</h2>
    <h3>{resposta}</h3>
  </main>
  );
};
