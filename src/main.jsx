import React from 'react'
import ReactDOM from 'react-dom'

import { BrowserRouter, Routes, Route } from "react-router-dom";

import 'bootstrap/dist/css/bootstrap.min.css';
import 'react-toastify/dist/ReactToastify.css'

import './index.css'
import App from './App'


import Inici from "./elements/Inici";
import Secret from "./elements/Secret";
import TramitePedido from "./elements/TramitePedido";
import SubirArticulo from "./elements/SubirArticulo";
import StockFotos from "./elements/StockFotos";
import Detall from "./elements/Detall";
import ModalEliminar from './elements/ModalEliminar';




ReactDOM.render(
  <BrowserRouter>
    <Routes>
      <Route path="/" element={<App />}>
        <Route index element={<Inici />} />
        <Route path="secret" element={<Secret />} />
        <Route path="tramitePedido" element={<TramitePedido />} />
        <Route path="subirArticulo" element={<SubirArticulo />} />
        <Route path="stockFotos" element={<StockFotos />} />
        <Route path="detall/:id" element={<Detall />} />
        <Route path="detall/:id" element={<ModalEliminar />} />
      </Route>
    </Routes>
  </BrowserRouter>,
  document.getElementById('root')
)
